#!/bin/bash
#
# written by sunweaver, taffit & h01ger 
#
# create edu-authors file from https://lists.debian.org/debian-edu/2013/10/msg00121.html

SVN_URL=svn+ssh://svn.debian.org/svn/debian-edu/
if [ "$1" = "" ] ; then
	PACKAGE=debian-edu-archive-keyring
else
	PACKAGE=$1
fi

PKGTEAM="" # could be set to "pkg-team/" for those packages...
case $PACKAGE in
    debian-edu|debian-edu-install)
        TRUNK=/branches/wheezy-proposed-updates/${PACKAGE}
    ;;
    # wheezy branch also got debian-edu and debian-edu-install
    debian-edu-artwork|debian-edu-config)
        TRUNK=/branches/wheezy/${PACKAGE}
    ;;
    *)
        TRUNK=/trunk/src/${PACKAGE}
    ;;
esac    

echo $PACKAGE 
echo $TRUNK
echo 


git svn clone --no-metadata \
    ${SVN_URL} \
    --trunk $TRUNK \
    --tags /tags/src/${PACKAGE} \
    --authors-file=$(dirname $0)/edu-authors \
    --prefix=svn-import/ \
    ${PACKAGE} 2>&1 | tee svn2git_${PACKAGE}.log


# convert the remote SVN tags to local Git tags
cd $PACKAGE

git for-each-ref --format="%(refname)" refs/remotes/svn-import/tags/ |
while read tag; do
    echo "Preserving tag \"$tag\""
    GIT_COMMITTER_DATE="$(git log -1 --pretty=format:"%ad" "$tag")" \
    GIT_COMMITTER_EMAIL="$(git log -1 --pretty=format:"%ce" "$tag")" \
    GIT_COMMITTER_NAME="$(git log -1 --pretty=format:"%cn" "$tag")" \
    git tag -m "$(git for-each-ref --format="%(contents)" "$tag")" \
                ${tag#refs/remotes/svn-import/tags/} "$tag"
done

cd ..

echo Now adding Alioth\'s git.debian.org as the origin remote...
git remote add origin ssh://git.debian.org/git/debian-edu/${PKGTEAM}/${PACKAGE}.git
echo Now run:
echo git push origin master
echo git push --tags origin
